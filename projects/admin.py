from django.contrib import admin
from .models import Project


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    class Meta:
        list_display = (
            "name",
            "description",
            "owner",
        )
